/*В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
* При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
* При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
* Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
* При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память. --------------------------------------------------------------------------

ранее не пользовалась памятью калькулятора, так что потестила на телефоне что оно делает:

m+ - добавляет в память число
m+ повторное - прибавляет то что на экране к числу в памяти
m- отнимает то что на экране от числа в памяти
mrc - отображает число из памяти
mrc повторное - стирает число из памяти
*/

function show(showenValue, directElement) {
	directElement.value = showenValue
}

function getNumer(value) {
	return Number(value);
}

function clearDisplay(valueItem) {
	return valueItem.value = "";
}
function selectOperator(operatorValue) {
	switch (operatorValue) {
		case "+":
			operator = "+";
			break;
		case "-":
			operator = "-";
			break;
		case "*":
			operator = "*";
			break;
		case "/":
			operator = "/";
			break;
	}
}

// Функції операторів ---
function mult(a, b) {
	return a * b
}
function divide(a, b) {
	return a / b
}
function add(a, b) {
	return a + b
}
function sub(a, b) {
	return a - b
}
//--------------------
let number1 = "";
let number2 = "";
let operator = "";
let result = "";
let memoryValue = "";
let mrcClicked = false;

window.addEventListener("DOMContentLoaded", () => {
	const btn = document.querySelector(".keys"),
		display = document.querySelector(".display > input"),
		memoryPlace = document.getElementById("memory");


	btn.addEventListener("click", function (e) {
		if (e.target.value === "+" || //   щоб на дисплеї не було непотрібного
			e.target.value === "-" ||
			e.target.value === "*" ||
			e.target.value === "/" ||
			e.target.value === "=" ||
			e.target.value === "m+" ||
			e.target.value === "m-" ||
			e.target.value === "mrc"
		) {
			display.value = display.value
		}
		else if (display.value === String(result) && result !== "") {
			clearDisplay(display);
			result = "";
			display.value += e.target.value

		}
		else {
			display.value += e.target.value;
		}


		show(display.value, display);

		if (e.target.value === "C") { // очищує дані
			display.value = "";
			number1 = "";
			number2 = "";
			operator = "";
			result = "";
		}

		if (e.target.value === "m+" && memoryValue === "") { // операції з пам'ятю
			memoryValue = getNumer(display.value);
			memoryPlace.innerText = "m"
		}
		else if (memoryValue !== "" && e.target.value === "m+") {
			memoryValue += getNumer(display.value)
		}

		if (memoryValue !== "" && e.target.value === "m-") {
			memoryValue -= getNumer(display.value)
		}
		if (e.target.value === "mrc" && memoryValue !== "" && mrcClicked === false) {
			display.value = memoryValue;
			show(display.value, display)
			mrcClicked = true;
		} else if (e.target.value === "mrc" && memoryValue !== "" && mrcClicked === true) {
			mrcClicked = false;
			memoryValue = "";
			memoryPlace.innerText = "";
			number1 = getNumer(display.value);
		}


		if (e.target.value === "+" ||
			e.target.value === "-" ||
			e.target.value === "*" ||
			e.target.value === "/"
		) {

			if (number1 === "") { // присвоюємо і показуэмо перше число
				number1 = getNumer(display.value)
				clearDisplay(display)
				operator = e.target.value
			}
			else if (number1 !== "" && number2 === "") { // присвоюємо друге число
				number2 = getNumer(display.value);

				switch (operator) {
					case "*": result = mult(number1, number2);
						display.value = result
						show(display.value, display)
						break;
					case "/": result = divide(number1, number2);
						show(result, display)
						break;
					case "+": result = add(number1, number2);
						show(result, display)
						break;
					case "-": result = sub(number1, number2);
						show(result, display);
						break;
				}                                    //виконуємо і показуємо дію
				operator = e.target.value;
				number1 = getNumer(result)
				number2 = "";

			}
		}
		if (e.target.value === "=") {
			//debugger
			number2 = getNumer(display.value);
			switch (operator) {
				case "*": result = mult(number1, number2);
					display.value = result
					show(display.value, display)
					number1 = getNumer(result);
					number2 = "";
					break;
				case "/": result = divide(number1, number2);
					show(result, display)
					number1 = getNumer(result);
					number2 = "";
					break;
				case "+": result = add(number1, number2);
					show(result, display)
					number1 = getNumer(result);
					number2 = "";
					break;
				case "-": result = sub(number1, number2);
					show(result, display)
					number1 = getNumer(result);
					number2 = "";
			}
			operator = "";
		}

	})
})
